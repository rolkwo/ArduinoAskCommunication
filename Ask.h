/*
 * Ask.h
 *
 *  Created on: Feb 27, 2017
 *      Author: roland
 */

#ifndef ASK_H_
#define ASK_H_


#include <Arduino.h>

#include <SoftwareSerial.h>

class Ask
{
public:
    Ask(uint8_t rxPin, uint8_t txPin, uint16_t baudrate);
    void send(uint8_t data);
    uint8_t receive(uint8_t* data, uint8_t bufLen);

//private:
    void sendStart();
    void sendStop();
    uint8_t rcvByte();
    bool convertToByte(uint16_t data, uint8_t& out);

    SoftwareSerial _serial;
};


#endif /* ASK_H_ */
