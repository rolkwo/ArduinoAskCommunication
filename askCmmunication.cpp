#include <Arduino.h>

#include "Ask.h"

Ask ask(8, 9, 300);

void setup() {
//	pinMode(9, OUTPUT);
}

void loop() {
	digitalWrite(9, LOW);
	ask.sendStart();
	ask.send(0xaa);
	ask.send(0x55);
	ask.sendStop();
	digitalWrite(9, LOW);

	delay(5000);
}
