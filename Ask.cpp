/*
 * Ask.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: roland
 */

#include "Ask.h"

#include <SoftwareSerial.h>

Ask::Ask(uint8_t rxPin, uint8_t txPin, uint16_t baudrate)
	: _serial(rxPin, txPin)
{
	_serial.begin(baudrate);
}

void Ask::send(uint8_t data)
{
    uint8_t bytes[2] = {0, 0};

    for(uint8_t bitNum = 0; bitNum < 8; ++bitNum)
    {
        uint8_t byteNum = bitNum / 4;
        if(data & (1 << bitNum))
        {
            bytes[byteNum] |= 0b01 << ((bitNum * 2) % 8);
        }
        else
        {
            bytes[byteNum] |= 0b10 << ((bitNum * 2) % 8);
        }
    }

    for(uint8_t i = 0; i < 2; ++i)
    {
    	_serial.write(bytes[i]);
    }
}

uint8_t Ask::receive(uint8_t* data, uint8_t bufLen)
{
    bool success = false;
    uint8_t pos = 0;

    do
    {
        uint8_t rcv = 0;
        do
        {
            rcv = rcvByte();
        }while(rcv != 0xf0);

        for(pos = 0; pos < bufLen; ++pos)
        {
            rcv = rcvByte();

            if(rcv == 0x0f)
                break;

            if(rcv == 0xf0)
            {
                success = false;
                pos = 0;
                continue;
            }

            uint16_t twoBytes = rcv;

            rcv = rcvByte();

            if(rcv == 0x0f)
            {
                success = false;
                break;
            }

            if(rcv == 0xf0)
            {
                success = false;
                pos = 0;
                continue;
            }

            twoBytes |= rcv << 8;

            success = convertToByte(twoBytes, data[pos]);
            if(!success)
                break;
        }

    }while(!success);

    return pos;
}

void Ask::sendStart()
{
    _serial.write(0xf0);
}

void Ask::sendStop()
{
    _serial.write(0x0f);
}

uint8_t Ask::rcvByte()
{
    while(!_serial.available())
        ;

    return _serial.read();
}

bool Ask::convertToByte(uint16_t data, uint8_t& out)
{
    for(uint8_t i = 0; i < 8; ++i)
    {
        if((data & 0x3) == 0b01)
            out |= 1 << i;
        else if((data & 0x03) == 0b10)
            out &= ~(1 << i);
        else
            return false;

        data >>= 2;
    }

    return true;
}
